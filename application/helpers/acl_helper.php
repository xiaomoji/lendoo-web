<?php
use \LeanCloud\User;

function acl () {
	if (User::getCurrentUser() != null && User::getCurrentUser()->get('username') == 'zhangsan') {
		return true;
	}
	return false;
}